<?php

//The test assignment 2

$a = ["dog", "cat", "mouse", "horse", "bird", "fish", "moose", "rabbit", "flower", "taco", "smile"];
$b = ["John", "Tom", "flower", "taco", "smile", "Matt", "Mark", "Luke", "David", "Sarah", "Beth", "Betty"];

//Standard Merge
$standardmerge = array_merge($a, $b);
//Unique Merge
$uniquemerge = array_unique($standardmerge);
//Alternate Merge
$alternatemerge = [];
if(count($a)>=count($b)) {
    foreach($a as $key => $value){
        if($b[$key]) array_push($alternatemerge,$value,$b[$key]); else array_push($alternatemerge,$value);
    }
} else {
    foreach($b as $key => $value){
        if($a[$key]) array_push($alternatemerge,$a[$key],$value); else array_push($alternatemerge,$value);
    }
}

var_dump($standardmerge);
echo "<br/><br/>";
var_dump($uniquemerge);
echo "<br/><br/>";
var_dump($alternatemerge);



?>
